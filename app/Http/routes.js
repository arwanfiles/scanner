'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.get('scan', 'ScanController.scan')
Route.get('convert', 'ScanController.convert')
Route.get('watch', 'ScanController.startwatch')
Route.get('stop', 'ScanController.stopwatch')
