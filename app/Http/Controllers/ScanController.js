'use strict'
const Helpers = use('Helpers')
const path = use('path')
const Chokidar = use('chokidar')
const Tesseract = use('tesseract.js')
const exec = use('child_process').execFile

class ScanController{

	*scan(request, response) {
		var plustekApp = Helpers.publicPath('plustekscan/PlkScan.exe')
		var savePath = path.join(Helpers.storagePath('/'))
		exec(plustekApp, ['/SaveToPath', savePath], function(err, data) {  
			console.log('Error :'+ err);
			console.log('Scanned');
		})
		return response.json({})
	}

	*convert(request, response) {
		var image = Helpers.storagePath('ktp.jpg')
		Tesseract.recognize(image)
	    .progress(function(p){console.log(p)})
	    .then( data => {
	    	console.log(data.text)
	    });
	    return response.json({})
	}

	*startwatch(request,  response){
		var dir = Helpers.storagePath()
		Chokidar.watch(dir, {ignored: /(^|[\/\\])\../}).on('all', (event, path) => {
			console.log(event, path);
		});
		return response.json({})
	}
	*stopwatch(request,  response){

	}

}

module.exports = ScanController